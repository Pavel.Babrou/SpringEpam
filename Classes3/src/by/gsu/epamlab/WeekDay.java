package by.gsu.epamlab;

public enum WeekDay {
    MONDAY("Monday"),
    TUEDAY("Tueday"),
    WEDNESDAY("Mednesday"),
    THRUSDAY("Thrusday"),
    FRIDAY("Friday"),
    SATURDAY("Saturday"),
    SUNDAY("Sunday");

    private String name;

    WeekDay(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
