package by.gsu.epamlab;

import org.omg.CosNaming.NamingContextExtPackage.StringNameHelper;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;


public class Runner {
    WeekDay weekDay;
    public static void main (String[] args) {
        Scanner sc = null;

        try {
            sc = new Scanner(new FileReader("src/in.txt"));
            sc.useLocale(Locale.ENGLISH);
            final int PURCAHSES_NUMBER = sc.nextInt();
            Purchase[] purchases = new Purchase[PURCAHSES_NUMBER];
            int i = 0;
            while (sc.hasNext()){
                int number = sc.nextInt();
                int percent = sc.nextInt();
                int day = sc.nextInt();
                purchases[i] = new Purchase(number, percent, day);
                i++;
            }

            System.out.println("Purchases units = " +PURCAHSES_NUMBER);
            printPurchase(purchases);
            getInfo(purchases);
            Arrays.sort(purchases);
            printPurchase(purchases);

            Purchase searchVal = new Purchase(5, 0, 0);
            int index = Arrays.binarySearch(purchases, searchVal);
            if(index >= 0) {
                System.out.println("Index purchase with units = 5: "+index+"; purchase "+purchases[index]+"\n");
            } else {
                System.out.println("Required purchase is not found");
            }



        } catch (FileNotFoundException e) {
            System.err.println("Input file is not found");
        } finally {
            if(sc != null) {
                sc.close();
            }
        }
    }
    public static void printPurchase(Purchase[] purchases){
        System.out.println("Purchase name = " + Purchase.getNAME());
        System.out.println("Purchase name = " + Purchase.getPRICE());
        for(Purchase p : purchases){
            System.out.println(p);
        }
    }
    public static void getInfo(Purchase[] purchases){
        int summ = 0;
        int monday = 0;
        int max = 0;
        int dayMaxCost = 0;
        for(Purchase p: purchases){
            summ += p.getCost();
            if(p.getDay() == 0){
                monday+=p.getCost();
            }
            if(p.getCost() > max){
                max = p.getCost();
                dayMaxCost = p.getDay();
            }
        }

        System.out.println("Average cost - " + summ/purchases.length);
        System.out.println("Monday - " + monday);
        System.out.println("the day with the maximum of purchases costs - " + dayMaxCost);
    }
}

