package by.gsu.epamlab;

public class Purchase implements Comparable<Purchase> {
    private static final String NAME = "Meat";
    private static final int PRICE = 50;
    private int number;
    private int percent;
    private int day;

    public Purchase(int number, int percent, int day) {
        this.number = number;
        this.percent = percent;
        this.day = day;
    }

    public static String getNAME() {
        return NAME;
    }

    public static int getPRICE() {
        return PRICE;
    }

    public Purchase() {
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getPercent() {
        return percent;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getCost(){
       return PRICE*number*(100 - percent)/100;
    }

    @Override
    public String toString(){
        return NAME +";"+PRICE+";"+number+";"+percent+";"+day+";"+getCost();
    }

    @Override
    public int compareTo(Purchase p) {
        return number > p.number ? 1: number == p.number ? 0: -1;
    }
}
