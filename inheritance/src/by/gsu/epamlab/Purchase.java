package by.gsu.epamlab;

import java.awt.*;
import java.util.Scanner;

public class Purchase {

    private String name;
    private Byn price;
    private int number;

    public Purchase() {
    }

    public Purchase(String name, Byn price, int number) {
        this.name = name;
        this.price = price;
        this.number = number;
    }
    public Purchase(Scanner sc){
        this(sc.next(), new Byn(sc.nextInt()), sc.nextInt());
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Byn getPrice() {
        return price;
    }
    public void setPrice(Byn price) {
        this.price = price;
    }
    public int getNumber() {
        return number;
    }
    public void setNumber(int number) {
        this.number = number;
    }
    public Byn getCost(){
        return new Byn(price).mul(number);
    }
    @Override
    public boolean equals(Object obj) {
        Purchase p = (Purchase) obj;
        return this.price.equals(p.price);
    }
    protected String fieldsToString() {
        return name +";"+ price +";"+ number;
    }
    @Override
    public String toString() {
        return fieldsToString() +";" +getCost();
    }
}
