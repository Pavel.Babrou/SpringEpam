package by.gsu.epamlab;

public class Byn implements Comparable<Byn> {
    Rounding rounding;
    private int value;

    public Byn() {
    }
    public Byn(int coin) {
        this.value = coin;
    }
    public Byn(int rub, int coin){
        this(rub*100 + coin);
    }
    public Byn (Byn byn){
        this(byn.value);
    }



    public int getRubs(){
        return value/100;
    }
    public int getCoins(){
        return value - getRubs()*100;
    }

    public Byn add (Byn byn){
        this.value += byn.value;
        return this;
    }
    public Byn sub (Byn byn){
        this.value -= byn.value;
        return this;
    }
    public Byn mull(int k){
        this.value *= k;
        return this;
    }
    public Byn mul(double k){
        this.value = (int) Math.round(value*k);
        return this;
    }
    public Byn mul(double k,int d){
        value = rounding.round(value * k, d);
        return this;
    }
    public Byn round(int d){
        value = rounding.round(value,  d);
        return this;
    }




    @Override
    public boolean equals(Object obj) {
        Byn byn = (Byn) obj;
        return this.value == byn.value;
    }
    @Override
    public String toString() {
        return value+"";
    }
    @Override
    public int compareTo(Byn byn) {
        return this.value - byn.value;
    }
}
