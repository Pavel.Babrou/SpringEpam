import by.gsu.epamlab.*;
import org.omg.CORBA.PUBLIC_MEMBER;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.InputMismatchException;
import java.util.Locale;
import java.util.Scanner;

public class Runner {
    public static void main(String[] args) {
        Scanner sc = null;

        try {
            sc = new Scanner(new FileReader("src/in.txt"));
            sc.useLocale(Locale.ENGLISH);
            final int PURCAHSES_NUMBER = 6;

            // Iten #1
            Purchase[] purchases = new Purchase[PURCAHSES_NUMBER];

            // Item #2
            Byn maxCost = new Byn();
            Purchase purchaseMaxCost = new Purchase();
            boolean areEqual = true;
            for (int i = 0; i < PURCAHSES_NUMBER;i++){
                purchases[i] = PurchaseFactory.getPurchaseFromFactory(sc);
                System.out.println(purchases[i]);
                Byn cost = purchases[i].getCost();
                if(cost.compareTo(maxCost) > 0) {
                    maxCost = cost;
                    purchaseMaxCost = purchases[i];
                }
                if (areEqual){
                    areEqual = purchases[i].equals(purchases[0]);
                    System.out.println(areEqual);
                }
            }
            System.out.println("The purchase with maximum cost - " + purchaseMaxCost);
            System.out.println("All purchases are equal is - " + areEqual);


        } catch (FileNotFoundException e) {
            System.err.println("Input file is not found");
        }
        finally {
            if(sc != null) {
                sc.close();
            }
        }
    }
}

