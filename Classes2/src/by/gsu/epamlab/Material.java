package by.gsu.epamlab;

public class Material {
    private final String name;
    private final double density;

    public Material(String name, double density) {
        this.density = density;
        this.name = name;
    }

    public Material() {
        this(null, 0.0);
    }

    public String getName() {
        return name;
    }

    public double getDensity() {
        return density;
    }

    @Override
    public String toString(){
        return name +";" + density;
    }
}
