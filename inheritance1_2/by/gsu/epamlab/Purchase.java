package by.gsu.epamlab;
import java.util.Scanner;

public class Purchase {
    private String name;
    private Byn price;
    private int number;

    public Purchase() {
    }

    public Purchase(String name, Byn price, int number) {
        this.name = name;
        this.price = price;
        this.number = number;
    }
    public Purchase(Scanner sc){
        this(sc.next(), new Byn(sc.nextInt()), sc.nextInt());
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Byn getPrice() {
        return price;
    }
    public void setPrice(Byn price) {
        this.price = price;
    }
    public int getNumber() {
        return number;
    }
    public void setNumber(int number) {
        this.number = number;
    }
    public Byn getCost(){
        Byn result = new Byn(price).mul(number);
        return result;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof Purchase))
            return false;
        Purchase other = (Purchase) obj;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (price == null) {
            if (other.price != null)
                return false;
        }else if (!price.equals(other.price))
            return false;
        return true;
    }
    protected String fieldsToString() {
        return name +";"+ price +";"+ number;
    }
    @Override
    public String toString() {
        return fieldsToString() +";" +getCost();
    }
}
