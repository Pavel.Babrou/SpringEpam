package by.gsu.epamlab;

import java.util.Scanner;

public class PercentDiscountPurchase extends Purchase {
    private final int LUCKY_NUMBER = 10;
    private double percentDiscount;

    public PercentDiscountPurchase(String name, Byn price, int number, double percentDiscount) {
        super(name, price, number);
        this.percentDiscount = percentDiscount;
    }

    public PercentDiscountPurchase(Scanner sc) {
        super(sc);
        this.percentDiscount = sc.nextDouble();
    }

    @Override
    public Byn getCost() {
        Byn cost = super.getCost();
        if(getNumber() > LUCKY_NUMBER){
            cost.mul(1- percentDiscount/100);
        }
        return cost;
    }

    protected String fieldsToString() {
        return super.fieldsToString() +";"+ percentDiscount;
    }

    @Override
    public String toString() {
        return fieldsToString() +";"+ getCost();
    }
}
