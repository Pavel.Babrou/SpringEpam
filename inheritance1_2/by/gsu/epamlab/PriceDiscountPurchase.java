package by.gsu.epamlab;

import java.util.Scanner;

public class PriceDiscountPurchase extends Purchase {
    private Byn priceDiscount;

    public PriceDiscountPurchase(Byn priceDiscount) {
        this.priceDiscount = priceDiscount;
    }

    public PriceDiscountPurchase(String name, Byn price, int number, Byn priceDiscount) {
        super(name, price, number);
        this.priceDiscount = new Byn(priceDiscount);
    }

    public PriceDiscountPurchase(Scanner sc) {
        super(sc);
        this.priceDiscount = new Byn(sc.nextInt());
    }

    public Byn getPriceDiscount() {
        return priceDiscount;
    }

    public void setPriceDiscount(Byn priceDiscount) {
        this.priceDiscount = priceDiscount;
    }

    @Override
    public Byn getCost() {
        return getPrice().sub(priceDiscount).mul(getNumber());
    }

    protected String fieldsToString() {
        return super.fieldsToString() +";"+ priceDiscount;
    }

    @Override
    public String toString() {
        return fieldsToString() +";"+ getCost();
    }
}
