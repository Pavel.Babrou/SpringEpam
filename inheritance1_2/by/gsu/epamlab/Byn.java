package by.gsu.epamlab;

public class Byn implements Comparable<Byn> {
    private int value;

    public Byn(int value) {
        this.value = value ;
    }
    public Byn(int rub, int coin){
        this.value = rub*100 + coin;
    }
    public Byn (Byn byn){
        this.value = byn.value;
    }

    public int getRubs(){
        return value/100;
    }
    public int getCoins(){
        return value / 10 % 10 + value %10;
    }
    public Byn add (Byn byn){
        return new Byn(this.value + byn.value);
    }
    public Byn add(int byn) {
        return new Byn(this.value+byn);
    }

    public Byn sub (Byn byn){
        return new Byn(this.value - byn.value);
    }
    public Byn sub(int byn) {
        return new Byn(this.value - byn);
    }
    public Byn mul(Byn byn) {
        return new Byn(this.value * byn.value);
    }
    public Byn mul(int k){
        return new Byn((int)(this.value * k));
    }
    public Byn mul(double k){
        this.value = (int) Math.round(value*k);
        return this;
    }
    public Byn round(int scale) {
        return new Byn((int)(this.value/Math.pow(10, scale)));
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Byn other = (Byn) obj;
        if (value != other.value)
            return false;
        return true;
    }

    public static String intToByn(int coin) {
        String result = coin / 100 + "." + coin / 10 % 10 + coin %10;
        return result;
    }
    @Override
    public String toString() {
//        return getRubs() +"."+getRubs();
        return intToByn(value);
    }
    @Override
    public int compareTo(Byn byn) {
        return this.value - byn.value;
    }
}
