package by.gsu.epamlab;

public enum Rounding {
    UP {
        @Override
        double roundFunction(double num) {
            return (int) Math.ceil(num);
        }
    },

    DOWN {
        @Override
        double roundFunction(double num) {
            return (int) Math.floor(num);
        }
    },

    NEAREST {
        @Override
        double roundFunction(double num) {
            return (int) Math.round(num);
        }
    };
    abstract double roundFunction(double arg);

    public int round(double roundedValue, int d) {
        int tenPow = (int) Math.pow(10, d);
        int result = (int) roundFunction(roundedValue/tenPow) * tenPow;
        return result;
    }
}

