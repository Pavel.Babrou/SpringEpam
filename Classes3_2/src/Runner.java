import by.gsu.epamlab.Purchase;
import by.gsu.epamlab.WeekDay;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

import static by.gsu.epamlab.Util.getByn;

public class Runner {
    public static void main (String[] args) {
        Scanner sc = null;

        try {
            sc = new Scanner(new FileReader("src/in.txt"));
            sc.useLocale(Locale.ENGLISH);
            final int PURCAHSES_NUMBER = sc.nextInt();

            // Iten #1
            Purchase[] purchases = new Purchase[PURCAHSES_NUMBER];

            // Item #2
            for (int k = 0; k < PURCAHSES_NUMBER; k++){
                int number = sc.nextInt();
                int percent = sc.nextInt();
                WeekDay day = getWeekDay(sc.nextInt());
                purchases[k] = new Purchase(number, percent, day);
            }

            //Item #3
            printPurchase(purchases);

            //Item #4
            getInfo(purchases);

            //Item #5
            Arrays.sort(purchases);

            //Item #6
            for(Purchase p: purchases){
                System.out.println(p);
            }

            //item #7
            Purchase searchVal = new Purchase(5, 0, null);
            int index = Arrays.binarySearch(purchases, searchVal);
            if(index >= 0) {
                System.out.println("Index purchase with units = 5: "+index+"; purchase "+purchases[index]+"\n");
            } else {
                System.out.println("Required purchase is not found");
            }

        } catch (FileNotFoundException e) {
            System.err.println("Input file is not found");
        } finally {
            if(sc != null) {
                sc.close();
            }
        }
    }


    private static void printPurchase(Purchase[] purchases){
        System.out.println("Purchase name = " + Purchase.NAME);
        System.out.println("Purchase cost = " + getByn(Purchase.PRICE));
        for(Purchase p : purchases){
            System.out.println(p);
        }
    }
    private static void getInfo(Purchase[] purchases){
        Purchase purchase = new Purchase();
        int summ = 0;
        double meanCost = 0;
        int monday = 0;
        double max = 0.00;
        WeekDay dayMaxCost = null;
        for(Purchase p: purchases){
            int cost = p.getCost();
            summ += cost;
            if(p.getDay() == WeekDay.MONDAY){
                monday+=cost;
            }
            if(cost > max){
                max = cost;
                dayMaxCost = p.getDay();
            }
        }

        if(purchases.length > 0){
            meanCost = (double) summ/purchases.length;
        }
        String averageCost = String.format("%.3f", meanCost/100);
        System.out.println("Mean cost - " + averageCost);
        System.out.println("Monday - " + getByn(monday));
        System.out.println("The day with the maximum purchase cost - " + dayMaxCost);
    }
    }
}
