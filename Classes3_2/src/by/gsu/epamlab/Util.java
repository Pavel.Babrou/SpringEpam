package by.gsu.epamlab;

public class Util {
    public static String getByn(int coin) {
        final String DOT = ".";
        String result = String.format("%d%s%02d", coin/100, DOT, coin%100);
        return result;
    }
}
