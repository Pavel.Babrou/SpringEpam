package by.gsu.epamlab;

public enum WeekDay {
    MONDAY,
    TUEDAY,
    WEDNESDAY,
    THRUSDAY,
    FRIDAY,
    SATURDAY,
    SUNDAY;

    @Override
    public String toString(){
        return this.name().toLowerCase();
    }
}
