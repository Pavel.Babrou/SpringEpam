package by.gsu.epamlab;

public class Purchase implements Comparable<by.gsu.epamlab.Purchase> {
    public static final String NAME = "Meat";
    public static final int PRICE = 5123;
    private int number;
    private double percent;
    private WeekDay day;

    public Purchase(int number, double percent, WeekDay day) {
        this.number = number;
        this.percent = percent;
        this.day = day;
    }
    public Purchase(int number, double percent, int day) {
        this(number, percent, WeekDay.values()[day]);
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public double getPercent() {
        return percent;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }

    public WeekDay getDay() {
        return day;
    }

    public void setDay(WeekDay day) {
        this.day = day;
    }

    public int getCost(){
        return (int) (Math.round(PRICE*number*(100 - percent)/100/100))*00;
    }

    @Override
    public String toString(){
        return number+";"+percent+";"+day.name();
    }

    @Override
    public int compareTo(by.gsu.epamlab.Purchase p) {
        return number - p.number;
    }
}
