import by.gsu.epamlab.*;

import java.util.Arrays;

public class Runner {
    public static void main(String[] args) {

        // Item #1:  Create the unique product for purchasing.
        final Product PRODUCT = new Product("Milk", new Byn(100));

        // Item #2:  Create an array for 6 objects (2 instances of every subclass by general-purpose constructors).
        AbstractPurchase[] purchases = {new PercentDiscountPurchase(PRODUCT, 5, 0),
                                      new PercentDiscountPurchase(PRODUCT, 50, 12.3),
                                      new PriceDiscountPurchase(PRODUCT, 50, 0),
                                      new PriceDiscountPurchase(PRODUCT, 11,25),
                                      new FarePurchase(PRODUCT, 11, new Byn(58)),
                                      new FarePurchase(PRODUCT, 15, new Byn(122))};

//        AbstractPurchase[] purchases = {new PercentDiscountPurchase(PRODUCT, 12, 2.32),
//                new PercentDiscountPurchase(PRODUCT, 50, 12.3),
//                new PriceDiscountPurchase(PRODUCT, 50, 0),
//                new PriceDiscountPurchase(PRODUCT, 11,25),
//                new FarePurchase(PRODUCT, 11, new Byn(58)),
//                new FarePurchase(PRODUCT, 5, new Byn(0))};

//        AbstractPurchase[] purchases = {new PercentDiscountPurchase(PRODUCT, 13, 5.75),
//                new PercentDiscountPurchase(PRODUCT, 50, 12.3),
//                new PriceDiscountPurchase(PRODUCT, 50, 0),
//                new PriceDiscountPurchase(PRODUCT, 5,0),
//                new FarePurchase(PRODUCT, 11, new Byn(58)),
//                new FarePurchase(PRODUCT, 15, new Byn(122))};

//        AbstractPurchase[] purchases = {new PercentDiscountPurchase(PRODUCT, 22, 3.25),
//                new PercentDiscountPurchase(PRODUCT, 12, 12.3),
//                new PriceDiscountPurchase(PRODUCT, 24, 0),
//                new PriceDiscountPurchase(PRODUCT, 11,25),
//                new FarePurchase(PRODUCT, 11, new Byn(58)),
//                new FarePurchase(PRODUCT, 15, new Byn(122))};

        // Item #3:  Print the array content to the console (one element per line).
        printArray(purchases);
        System.out.println("**********************************");

        // Item #4:  Sort an array by the cost in descending order by the method sort( ) of the class Arrays.
        Arrays.sort(purchases);

        // Item #5:  Print the array content to the console (one element per line).
        printArray(purchases);
        System.out.println("**********************************");

        // Item #5:  Print the minimum cost of purchase.
        getInfo(purchases);
        System.out.println("**********************************");


        // Item #7:  Find some purchase with cost equaled to 5.00 BYN by the method binarySearch( ) of the class Arrays and print it.
        Arrays.sort(purchases);
        AbstractPurchase desiredPurchase = new FarePurchase(new Product("test",new Byn(100)),5, new Byn(0));
//        AbstractPurchase desiredPurchase = new FarePurchase(new Product("test",new Byn(50000)),5, new Byn(0));
        int id = Arrays.binarySearch(purchases, desiredPurchase);
        if(id >= 0) {
            System.out.println("Purchase with cost = 5.00; "+purchases[id]);
        } else {
            System.out.println("Required purchase is not found");
        }
        System.out.println("**********************************");
    }

    private static void printArray(AbstractPurchase[] purchases){
        for(AbstractPurchase p: purchases){
            System.out.println(p);
        }
    }
    public static void getInfo(AbstractPurchase[] purchases){
        Byn minCost = purchases[0].getCost();
        for(AbstractPurchase p: purchases){
            Byn cost = p.getCost();
            if(cost.compareTo(minCost) < 0) {
                minCost = cost;
            }
        }
        System.out.println("Minimum cost = " +  minCost);
    }
}
