package by.gsu.epamlab;

public class FarePurchase extends AbstractPurchase {
    private Byn fare;

    public FarePurchase(Byn fare) {
        this.fare = fare;
    }
    public FarePurchase(Product product, int number, Byn fare) {
        super(product, number);
        this.fare = fare;
    }

    @Override
    public String fieldToString() {
        return super.fieldToString() +";"+ fare;
    }
    @Override
    public String toString() {
        return fieldToString() +";"+getCost();
    }
    @Override
    public Byn getDiscount() {
        return new Byn(fare.mul(1));
    }
}
