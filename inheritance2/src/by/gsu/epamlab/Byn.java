package by.gsu.epamlab;

public final class Byn implements Comparable <Byn> {
    private final int VALUE;

    public Byn() {
        this(0);
    }
    public Byn(int coins) {
        this.VALUE = coins;
    }
    public Byn(double coins) {
        this.VALUE = (int) coins;
    }
    public Byn(int rub, int coins) {
        this(rub * 100 + coins);
    }
    public Byn(Byn byn) {
        this(byn.VALUE);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Byn other = (Byn) obj;
        if (VALUE != other.VALUE)
            return false;
        return true;
    }
    @Override
    public int compareTo(Byn byn) {
        return this.VALUE - byn.VALUE;
    }
    @Override
    public String toString() {
        return intToByn(VALUE);
    }
    private static String intToByn(int cent) {
        final String OUT_SEPARATOR = ".";
        String result = cent / 100 + OUT_SEPARATOR + cent / 10 % 10 + cent %10;
        return result;
    }
    public int getRubs() {
        return VALUE / 100;
    }
    public int getCoins() {
        return this.VALUE - this.getRubs() * 100;
    }
    public Byn add(Byn byn) {
        return new Byn(this.VALUE + byn.VALUE);
    }
    public Byn add(Byn byn, int scale) {
        return new Byn(round(this.VALUE + byn.VALUE, Rounding.NEAREST, scale));
    }
    public Byn add(Byn byn, Rounding rounding, int scale) {
        double roundedValue = rounding.roundFun(this.VALUE + byn.VALUE);
        int result;
        if(scale != 0) {
            result = round(roundedValue, rounding, scale);
        } else {
            result = (int) roundedValue;
        }
        return new Byn(result);
    }

    public Byn sub(Byn byn) {
        return new Byn(this.VALUE - byn.VALUE);
    }
    public Byn sub(Byn byn, int scale) {
        return new Byn(round(this.VALUE - byn.VALUE, Rounding.NEAREST, scale));
    }
    public Byn sub(Byn byn, Rounding rounding, int scale) {
        return this.add(new Byn(-byn.VALUE), rounding, scale);
    }
    public Byn mul(double coef, Rounding rounding, int scale) {
        double roundedValue = rounding.roundFun(this.VALUE * coef);
        int result;
        if(scale != 0) {
            result = round(roundedValue, rounding, scale);
        } else {
            result = (int) roundedValue;
        }
        return new Byn(result);
    }
    public Byn mul(double coef, Rounding rounding) {
        return this.mul(coef, rounding, 0);
    }
    public Byn mul(double coef) {
        return this.mul(coef, Rounding.NEAREST);
    }
    public Byn mul(int coef) {
        return new Byn(this.VALUE * coef);
    }
    public Byn div(double coef, Rounding rounding, int scale) {
        return this.mul(1 / coef, rounding, scale);
    }
    public Byn div(double coef, Rounding rounding) {
        return this.mul(1 / coef, rounding, 0);
    }
    public Byn div(double coef) {
        return this.mul(1 / coef, Rounding.NEAREST);
    }
    private static int round (double roundedValue, Rounding rounding, int scale) {
        int tenPow = tenPow(scale);
        return (int) rounding.roundFun(roundedValue / tenPow) * tenPow;
    }
    private static int tenPow(int scale) {
        int tenPow;
        if (scale < 1) {
            tenPow = 1;
        } else {
            scale--;
            tenPow = 10 * tenPow(scale);
        }
        return tenPow;
    }
}
