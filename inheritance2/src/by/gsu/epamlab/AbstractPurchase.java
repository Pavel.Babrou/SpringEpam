package by.gsu.epamlab;

public abstract class AbstractPurchase implements Comparable<AbstractPurchase>{
    private Product product;
    private int number;

    public AbstractPurchase() {
    }
    public AbstractPurchase(Product product, int number) {
        super();
        this.product = product;
        this.number = number;
    }

    public Product getProduct() {
        return product;
    }
    public void setProduct(Product product) {
        this.product = product;
    }
    public int getNumber() {
        return number;
    }
    public void setNumber(int number) {
        this.number = number;
    }
    public static double roundDown(double number){
        return Math.floor(number);
    }

    public Byn getCost() {
        Byn cost = product.getPrice().mul(number).sub(getDiscount(), Rounding.DOWN, 2);
        return cost;
    }


    public String fieldToString(){
        return product +";"+ number;
    }

    @Override
    public String toString() {
        return fieldToString() +";"+ getCost();
    }
    @Override
    public int compareTo(AbstractPurchase p) {
        return -this.getCost().compareTo(p.getCost());
    }
    protected abstract Byn getDiscount();
}
