package by.gsu.epamlab;

public enum Rounding {
    UP {
        @Override
        double roundFun(double roundedValue) {
            return (int) Math.ceil(roundedValue);
        }
    },
    DOWN {
        @Override
        double roundFun(double roundedValue) {
            return (int) Math.floor(roundedValue);
        }
    },
    NEAREST {
        @Override
        double roundFun(double roundedValue) {
            return (int) Math.round(roundedValue);
        }
    },
    DISCARD {
        @Override
        double roundFun(double roundedValue) {
            return (int) roundedValue;
        }
    };

    abstract double roundFun(double roundedValue);
}