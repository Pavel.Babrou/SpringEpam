package by.gsu.epamlab;

public class Product {
    private String NAME;
    private Byn PRICE;

    public Product() {
    }
    public Product(String name, Byn price) {
        this.NAME = name;
        this.PRICE = price;
    }

    public String getName() {
        return NAME;
    }
    public void setName(String name) {
        this.NAME = name;
    }
    public Byn getPrice() {
        return PRICE;
    }
    public Byn getCost(){
        return PRICE;
    }
    public void setPrice(Byn price) {
        this.PRICE = price;
    }

    @Override
    public String toString() {
        return NAME +";"+PRICE;
    }
}
