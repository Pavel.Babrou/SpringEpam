package by.gsu.epamlab;

public class PriceDiscountPurchase extends AbstractPurchase{

    private Byn priceDiscount;

    public PriceDiscountPurchase(Byn priceDiscount) {
        this.priceDiscount = priceDiscount;
    }
    public PriceDiscountPurchase(Product product, int number, int priceDiscount) {
        super(product, number);
        this.priceDiscount = new Byn(priceDiscount);
    }

    public Byn getPriceDiscount() {
        return priceDiscount;
    }
    public void setPriceDiscount(Byn priceDiscount) {
        this.priceDiscount = priceDiscount;
    }

    @Override
    public Byn getDiscount() {
        return priceDiscount.mul(getNumber());
    }
    @Override
    public String fieldToString() {
        return super.fieldToString() +";"+ priceDiscount;
    }
    @Override
    public String toString() {
        return fieldToString() +";"+ getCost();
    }
}
