package by.gsu.epamlab;

public class PercentDiscountPurchase extends AbstractPurchase{
    private final int LUCKY_NUMBER = 10;
    private double percentDiscount;

    public PercentDiscountPurchase(double percentDiscount) {
        this.percentDiscount = percentDiscount;
    }
    public PercentDiscountPurchase(Product product, int number, double percentDiscount) {
        super(product, number);
        this.percentDiscount = percentDiscount;
    }

    @Override
    public Byn getDiscount() {
        Byn discount;
        if (getNumber() > LUCKY_NUMBER) {
            discount = getProduct().getPrice().mul(getNumber() * percentDiscount / 100);
        } else {
            discount = new Byn(0);
        }
        return discount;
    }
    @Override
    public String fieldToString() {
        return super.fieldToString() +";"+ percentDiscount;
    }
    @Override
    public String toString() {
        return fieldToString() +";"+ getCost();
    }

}
