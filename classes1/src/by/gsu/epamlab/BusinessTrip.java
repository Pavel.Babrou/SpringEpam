package by.gsu.epamlab;

public class BusinessTrip {
    private static final int DAILY_ALLOWANCE_RATE = 700;
    private String account;
    private int transport;
    private int days;

    public BusinessTrip() {
    }

    public BusinessTrip(String account, int transport, int days) {
        this.account = account;
        this.transport = transport;
        this.days = days;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public int getTransport() {
        return transport;
    }

    public void setTransport(int transport) {
        this.transport = transport;
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }

    public int getTotal(){
        int total = transport+(DAILY_ALLOWANCE_RATE*days);
        return total;
    }
    private String formater(int digit) {
        String byn = (digit / 100) + "";
        String coin = (digit % 100) + "";
        if (coin.length() < 2)
            coin = "0" + coin;

        return byn + "." + coin;
    }


    public void show(){
        System.out.println("Account = "+ account + "\n"
        + "Rate = " + formater(DAILY_ALLOWANCE_RATE)+"\n"
        + "Transport = "+formater(transport) +"\n"
        + "Days = " + days + "\n"
        + "Total = " + formater(getTotal())+"\n");
    }

    @Override
    public String toString(){
        return account+";"+ formater(transport) + ";" + days  +";" + formater(getTotal());
    }
}
