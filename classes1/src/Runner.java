import by.gsu.epamlab.BusinessTrip;

public class Runner {
    public static void main(String[] args) {
        BusinessTrip[] businessTrips = {new BusinessTrip("Artem", 1105, 12),
                new BusinessTrip("Anton", 4130, 4),
                null,
                new BusinessTrip("Dmitry", 1823, 7),
                new BusinessTrip()};


        for(BusinessTrip trip: businessTrips){
            if(trip != null)
                trip.show();
        }

        businessTrips[businessTrips.length-1].setTransport(3338);
        int duration = businessTrips[0].getDays()+businessTrips[1].getDays();
        System.out.println("Duration = "+duration);

        for(BusinessTrip trip: businessTrips){
            System.out.println(trip);
        }
    }
}
